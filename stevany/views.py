from django.core.paginator import Paginator
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.db import connection
from michael.check_login import required_login
from .forms import FormBarang, InfoBarang, order_and_filter
from django.forms import formset_factory
from michael.models import LevelKeanggotaan, Barang, InfoBarangLevel, Chat,KategoriItem
from datetime import datetime
from django.contrib import messages



# Create your views here.


@required_login
def chat(request):
    no_ktp = request.session['no_ktp']
    if request.method == 'POST':
        latest_row = Chat.objects.raw('SELECT * from chat order by id::int DESC LIMIT 1')
        latest_id = int(latest_row[0].id)
        pesan = request.POST['pesan']
        if request.session['role'] == 'admin':
            user = request.GET.get('user-ktp')
            with connection.cursor() as cursor:
                    cursor.execute("INSERT INTO CHAT values (%s, %s, %s, %s, %s, %s)", [str(latest_id+1), pesan, datetime.now(), user, no_ktp, True])
        else:
            with connection.cursor() as cursor:
                    cursor.execute("INSERT INTO CHAT values (%s, %s, %s, %s, %s, %s)", [str(latest_id+1), pesan, datetime.now(), no_ktp, '1000009', False])
        return HttpResponseRedirect(request.META['HTTP_REFERER'])
    if request.session['role'] == 'admin':
        user = request.GET.get('user-ktp')
        chats = Chat.objects.raw('Select * from chat where no_ktp_anggota = %s', [user])
        return render(request, 'stevany/chat_anggota.html', {'chats': chats})
    elif request.session['role'] == 'anggota':
        chats = Chat.objects.raw('Select * from chat where no_ktp_anggota = %s', [no_ktp])
        return render(request, 'stevany/chat_anggota.html',  {'chats': chats})


@required_login
def list_chat_admin(request):
    if request.session['role'] == 'user':
        return HttpResponseRedirect(reverse('items:list-item'))
    with connection.cursor() as cursor:
        cursor.execute('SELECT distinct(no_ktp_anggota) from CHAT,pengguna where chat.no_ktp_anggota = pengguna.no_ktp')
        list_no = cursor.fetchall()
        list_chat = []
        for nomor in list_no:
            cursor.execute('Select nama_lengkap from pengguna where no_ktp = %s', [nomor])
            res = cursor.fetchall()
            res.append(nomor)
            list_chat.append(res)
    return render(request, 'stevany/chat_admin.html', {'chats': list_chat})



def save_toys(toys, info):
    toy = [toys.cleaned_data.get('id_barang'), toys.cleaned_data.get('nama_item'), toys.cleaned_data.get('warna'),
           toys.cleaned_data.get('url_foto'), toys.cleaned_data.get('kondisi'),
           toys.cleaned_data.get('lama_penggunaan'),
           toys.cleaned_data.get('no_ktp_penyewa')]
    with connection.cursor() as cursor:
        cursor.execute('INSERT INTO BARANG values (%s, %s, %s, %s, %s, %s, %s)', toy)
        for f in info.forms:
            result_info = [
                toys.cleaned_data.get('id_barang'),
                f.cleaned_data.get('nama_level'),
                f.cleaned_data.get('harga_sewa'),
                f.cleaned_data.get('porsi_royalti'),
            ]
            cursor.execute('INSERT INTO info_barang_level values (%s, %s, %s, %s)', result_info)


@required_login
def toys_add(request):
    if request.session['role'] == 'user':
        return HttpResponseRedirect(reverse('items:list-item'))
    hasil = LevelKeanggotaan.objects.raw('select nama_level from level_keanggotaan')
    jumlah = len(hasil)
    list_nama_level = [{'nama_level': n.nama_level} for n in hasil]
    formset = formset_factory(InfoBarang, extra=jumlah, max_num=jumlah)
    if request.method == 'POST':
        resform = FormBarang(request.POST, prefix="barang")
        resformset = formset(request.POST, prefix="info", initial=list_nama_level)
        if resform.is_valid() and resformset.is_valid():
            listnama = [l.nama_level for l in hasil]
            for f in resformset.forms:
                if f.cleaned_data.get('nama_level') not in listnama:
                    messages.error(request, "ERROR")
                    return HttpResponseRedirect(request.META['HTTP_REFERER'])
            try:
                save_toys(resform, resformset)
            except Exception:
                messages.error(request, "Check if id not duplicated")
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            return HttpResponseRedirect(reverse('toys:toys_list'))
    form = FormBarang(prefix="barang")
    forminfo = formset(prefix="info", initial=list_nama_level)
    return render(request, 'stevany/toys_add.html', {'form': form, 'formset': forminfo})


@required_login
def toys_list(request):
    form = order_and_filter(request.GET)
    item = request.GET.get('item')
    if not form.is_valid():
        messages.error(request, "ERROR")
        return HttpResponseRedirect(request.META['HTTP_REFERER'])
    order = form.cleaned_data.get('order')
    category = form.cleaned_data.get('filter_category')
    if item:
        query = 'Select * from barang where nama_item = %s'
        list_barang = Barang.objects.raw(query, [item])
    else:
        if category:
            query = 'SELECT * , id_barang from kategori_item, barang where nama_kategori = %s and barang.nama_item = kategori_item.nama_item'
        else:
            query = 'Select * from barang'
        if order == 'ascending':
            query += ' order by barang.nama_item ASC'
        elif order == 'descending':
            query += ' order by barang.nama_item DESC'
        if category:
            list_barang = KategoriItem.objects.raw(query, [category])
        else:
            list_barang = Barang.objects.raw(query)
    p = Paginator(list_barang, 10)
    page = request.GET.get("page")
    items = p.get_page(page)
    return render(request, 'stevany/toys_list.html', {'item': items, 'form': form})


@required_login
def toys_detail(request, id_barang):
    barang = Barang.objects.raw('Select * from barang where id_barang = %s', [id_barang])
    info = InfoBarangLevel.objects.raw('select * from info_barang_level where id_barang = %s', [id_barang])
    with connection.cursor() as cursor:
        cursor.execute(
            'Select barang_dikirim.review, barang_dikirim.tanggal_review from barang_dikirim where id_barang = %s',
            [id_barang])
        review = cursor.fetchall()
    formset = formset_factory(InfoBarang, extra=len(info), max_num=len(info))
    initial_barang = {
        'id_barang': barang[0].id_barang,
        'nama_item': barang[0].nama_item.nama,
        'warna': barang[0].warna,
        'url_foto': barang[0].url_foto,
        'kondisi': barang[0].kondisi,
        'lama_penggunaan': barang[0].lama_penggunaan,
        'no_ktp_penyewa': barang[0].no_ktp_penyewa.no_ktp.no_ktp
    }
    resform = FormBarang(prefix="barang", initial=initial_barang)
    for f in resform:
        f.field.disabled = True
    initial_info = [
        {
            'nama_level': i.nama_level.nama_level,
            'harga_sewa': i.harga_sewa,
            'porsi_royalti': i.porsi_royalti
        }
        for i in info
    ]
    resformset = formset(prefix="info", initial=initial_info)
    for f in resformset.forms:
        for fd in f:
            fd.field.disabled = True
    return render(request, 'stevany/toys_detail.html', {'form': resform, 'formset': resformset, 'review': review})


@required_login
def update_toys(request, id_barang):
    if request.session['role'] == 'user':
        return HttpResponseRedirect(reverse('items:list-item'))
    barang = Barang.objects.raw('Select * from barang where id_barang = %s', [id_barang])
    info = InfoBarangLevel.objects.raw('select * from info_barang_level where id_barang = %s', [id_barang])
    formset = formset_factory(InfoBarang, extra=len(info), max_num=len(info))
    initial_barang = {
        'id_barang': barang[0].id_barang,
        'nama_item': barang[0].nama_item.nama,
        'warna': barang[0].warna,
        'url_foto': barang[0].url_foto,
        'kondisi': barang[0].kondisi,
        'lama_penggunaan': barang[0].lama_penggunaan,
        'no_ktp_penyewa': barang[0].no_ktp_penyewa.no_ktp.no_ktp
    }
    initial_info = [
        {
            'nama_level': i.nama_level.nama_level,
            'harga_sewa': i.harga_sewa,
            'porsi_royalti': i.porsi_royalti
        }
        for i in info
    ]
    if request.method == 'POST':
        form = FormBarang(request.POST, prefix="barang", initial=initial_barang)
        forminfo = formset(request.POST, prefix="info", initial=initial_info)
        if form.is_valid() and forminfo.is_valid():
            if form.has_changed():
                for c in form.changed_data:
                    if c == 'id_barang':
                        id_barang = initial_barang['id_barang']
                    else:
                        id_barang = form.cleaned_data.get('id_barang')
                    data = [form.cleaned_data.get(c), id_barang]
                    with connection.cursor() as cursor:
                        cursor.execute(
                            'UPDATE barang set %s = %s WHERE id_barang = %s' % (c, '%s', '%s'), data
                        )
            if forminfo.has_changed():
                for f in forminfo:
                    if f.has_changed():
                        for c in f.changed_data:
                            data = [f.cleaned_data.get(c), form.cleaned_data.get('id_barang'), f.cleaned_data.get('nama_level')]
                            with connection.cursor() as cursor:
                                cursor.execute(
                                    'UPDATE info_barang_level set %s = %s WHERE id_barang = %s and nama_level = %s' %
                                    (c, '%s', '%s', '%s'), data
                                )
            return HttpResponseRedirect(reverse('toys:toys_list'))
    form = FormBarang(initial=initial_barang, prefix="barang")
    forminfo = formset(prefix="info", initial=initial_info)
    return render(request, 'stevany/toys_add.html', {'form': form, 'formset': forminfo})


@required_login
def delete_barang(request, id_barang):
    if request.session['role'] == 'user':
        return HttpResponseRedirect(reverse('items:list-item'))
    with connection.cursor() as cursor:
        cursor.execute(
            'DELETE FROM barang where id_barang = %s', [id_barang]
        )
    return HttpResponseRedirect(reverse('toys:toys_list'))