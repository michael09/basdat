from django.urls import path

app_name = 'toys'
from .views import *

urlpatterns = [
    path('add', toys_add, name='toys_add'),
    path('', toys_list, name='toys_list'),
    path('detail/<id_barang>', toys_detail, name='toys_detail'),
    path('update-barang/<id_barang>', update_toys, name='update-barang'),
    path('delete-barang/<id_barang>', delete_barang, name='delete-barang')
]
