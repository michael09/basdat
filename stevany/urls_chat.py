from django.urls import path

app_name = 'chat'
from .views import *

urlpatterns = [
    path('', chat, name='chat'),
    path('list-admin', list_chat_admin, name='list-chat')
]
