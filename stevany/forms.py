from django import forms
from django.db import connection


def list_pengguna():
    with connection.cursor() as cursor:
        cursor.execute(
            'SELECT Anggota.no_ktp, Pengguna.nama_lengkap from Anggota, Pengguna where Anggota.no_ktp = Pengguna.no_ktp'
            )
        row = cursor.fetchall()
        return row


def list_item():
    with connection.cursor() as cursor:
        cursor.execute(
            'SELECT nama, nama from item'
            )
        row = cursor.fetchall()
        return row


class FormBarang(forms.Form):
    id_barang = forms.CharField(widget=forms.NumberInput(attrs={'class': 'form-field flex-column'}), min_length=1)
    nama_item = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), choices=list_item())
    warna = forms.CharField(max_length=50, widget=forms.TextInput(attrs={'class': 'form-field flex-column'}), required=False)
    url_foto = forms.CharField(max_length=300, widget=forms.TextInput(attrs={'class': 'form-field flex-column'}), required=False)
    kondisi = forms.CharField(max_length=300, widget=forms.TextInput(attrs={'class': 'form-field flex-column'}))
    lama_penggunaan = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-field flex-column'}),
                                       required=False)
    no_ktp_penyewa = forms.ChoiceField(label="Pemilik Barang", widget=forms.Select(attrs={'class': 'form-control'}), choices=list_pengguna())


class InfoBarang(forms.Form):
    nama_level = forms.CharField(max_length=300, widget=forms.TextInput(attrs={'class': 'form-field flex-column',
                                                                           'readonly': True}))
    harga_sewa = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-field flex-column'}), min_value=1)
    porsi_royalti = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-field flex-column'}), min_value=0
                                        , max_value=100)


def listIDBarang():
    with connection.cursor() as cursor:
        cursor.execute(
            'SELECT barang.id_barang from barang'
        )
        row = cursor.fetchall()
        return row


class selectToy(forms.Form):
    id_barang = forms.CheckboxSelectMultiple(choices=listIDBarang())


def list_category():
    with connection.cursor() as cursor:
        cursor.execute(
            'SELECT nama, nama from kategori'
        )
        row = cursor.fetchall()
    row.append(('', ''))
    return row


class order_and_filter(forms.Form):
    filter_category = forms.ChoiceField(required=False, label="Filter Kategori", widget=forms.Select(attrs={'class': 'form-control','id': 'sel1'}), choices=list_category())
    order = forms.ChoiceField(required=False, label="order", widget=forms.Select(attrs={'class': 'form-control', 'id': 'sel1'}), choices=[('',''), ('ascending', 'ascending'), ('descending', 'descending')])

