from django.urls import path
app_name = 'shipping'
from .views import *

urlpatterns = [
	path('', shipping, name=''),
    path('add', shipping_add, name='add'),
    path('update', shipping_update, name='update'),
]