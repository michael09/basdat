from django.urls import path
app_name = 'review'
from .views import *

urlpatterns = [
	path('', review, name=''),
    path('add', review_add, name='add'),
    path('update', review_update, name='update'),
]