from django.forms import formset_factory
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from .forms import ShippingForm, ReviewForm
from django.db import connection
#from michael.check_login import required_login
from michael.models import Pengiriman
from django.contrib import messages
from django.core.paginator import Paginator
# Create your views here.

def shipping(request):
    return render(request, 'shipping/shipping.html')


def shipping_post(form):
    latest_row = Pengiriman.objects.raw('SELECT * from pengiriman order by no_resi::int DESC LIMIT 1')
    latest_no_resi = int(latest_row[0].no_resi)
    ongkos = 10000
    result = [latest_no_resi,
              form.cleaned_data.get('id_pemesanan'),
              form.cleaned_data.get('metode'),
              ongkos,
              form.cleaned_data.get('tanggal'),
              form.cleaned_data.get('nama_alamat'),
              ]

    with connection.cursor() as cursor:
        cursor.execute(("INSERT INTO pengiriman values(%s, %s, %s, %s, %s, %s)", result))

def shipping_add(request):
    role = request.GET.get('role')
    if role == 'anggota':
        if request.method == 'POST':
            form = ShippingForm(request.POST, prefix='form')
            if form.is_valid():
                pemesanan_terkirim = list(Pengiriman.objects.raw('SELECT id_pemesanan FROM PENGIRIMAN WHERE id_pemesanan = %s',
                                                                 [form.cleaned_data.get('id_pemesanan'), ]))
                if pemesanan_terkirim:
                    messages.error(request, 'Pemesanan sudah ada untuk dikirim!')

                else:
                    shipping_post(form)

    form = ShippingForm(prefix='form')
    return render(request, 'shipping/shipping_add.html')


def shipping_update_post(form, no_resi_arg):
    result = [form.cleaned_data.get('id_pemesanan'),
              form.cleaned_data.get('metode'),
              form.cleaned_data.get('tanggal'),
              form.cleaned_data.get('nama_alamat'),
              ]

    with connection.cursor() as cursor:
        cursor.execute(("UPDATE pengiriman SET id_pemesanan=%s, metode=%s, tanggal=%s, nama_alamat=%s WHERE no_resi=no_resi_arg", result))

def shipping_update(request):
    # harus ngeget no_resinya ini
    role = request.GET.get('role')
    if role == 'anggota':
        if request.method == 'POST':
            form = ShippingForm(request.POST, prefix='form')
            if form.is_valid():
                pemesanan_terkirim = list(Pengiriman.objects.raw("SELECT metode FROM PENGIRIMAN WHERE no_resi = %s AND metode='dikirim'",
                                                                 [form.cleaned_data.get('metode'), ]))
                if pemesanan_terkirim:
                    messages.error(request, 'Pemesanan sudah terkirim!')

                else:
                    no_resi = 0 # placeholder
                    shipping_update_post(form,no_resi)

    form = ShippingForm(prefix='form')
    return render(request, 'shipping/shipping_update.html')

''' ini bisa dihapus '''
def shipping_delete_post(no_resi_arg):
    with connection.cursor() as cursor:
        cursor.execute(("DELETE FROM pengiriman WHERE no_resi=no_resi_arg", result))

def shipping_delete(request):
    return render(request, 'shipping/shipping_delete.html')





def review(request):
    return render(request, 'review/review.html')

def review_add(request):
    role = request.GET.get('role')
    if role == 'anggota':
        if request.method == 'POST':
            form = ReviewForm(request.POST, prefix='form')
            if form.is_valid():
                review_exist = list(Pengiriman.objects.raw('SELECT no_urut FROM barang_dikirim WHERE no_urut = %s AND id_barang = %s',
                                                           [form.cleaned_data.get('no_urut'), ]))
                if review_exist:
                    messages.error(request, 'Review sudah ada!')
    form = ReviewForm(prefix='form')
    return render(request, 'review/review_add.html')

def review_update(request):
    return render(request, 'review/review_update.html')

def review_delete(request):
    return render(request, 'review/review_delete.html')
