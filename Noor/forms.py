from django import forms
from django_select2.forms import Select2MultipleWidget
from michael.models import Pemesanan, Alamat, Pengiriman
from django.core.validators import RegexValidator
#validate_regex = RegexValidator(r'[0-9]-[0-9]')


def list_choices_pemesanan():
	#query = "SELECT id_pemesanan FROM PEMESANAN A JOIN BARANG_PESANAN B ON A.id_pemesanan = b.id_pemesanan"
	result = Pemesanan.objects.raw("SELECT id_pemesanan FROM PEMESANAN")
	cleaned = [[i.id_pemesanan, i.id_pemesanan] for i in result]
	return cleaned

def list_choices_alamat():
	result = Alamat.objects.raw("SELECT * from ALAMAT")
	cleaned = [[i.jalan + ' ' + i.nomor + ' ' + i.kota + ' ' + i.kodepos,
	i.jalan + ' ' + i.nomor + ' ' + i.kota + ' ' + i.kodepos] for i in result]
	# cleaned = [[i.nama, i.nama] for i in result]
	return cleaned

class ShippingForm(forms.Form):
	# id_pemesanan = forms.MultipleChoiceField(required=True, label='Barang Pesanan', choices=list_choices_pemesanan,
	# 	widget=Select2MultipleWidget(attr={'class' : 'form-field flex-column'}))
	# nama_alamat = forms.MultipleChoiceField(required=True, label='Alamat Tujuan', choices=list_choices_alamat,
	# 	widget=Select2MultipleWidget(attr={'class' : 'form-field flex-column'}))
	tanggal = forms.DateField(required=True, label='Tanggal Pengiriman', widget=forms.DateInput(
		attrs={'type':'date'}))
	metode = forms.CharField(max_length=255, label='Metode', widget=forms.TextInput(
		attrs={'class': 'form-field flex-column'}))

# update kyknya pake form shipping krn sama juga isinya
# class UpdateShippingForm(forms.Form):

def list_choices_pengiriman():
	result = Pengiriman.objects.raw("SELECT no_resi FROM PENGIRIMAN")
	cleaned = [[i.no_resi, i.no_resi] for i in result]
	return cleaned

class ReviewForm(forms.Form):
	# id_barang = forms.MultipleChoiceField(required=True, label='Barang', choices=list_choices_pengiriman,
	# 	widget=Select2MultipleWidget(attr={'class' : 'form-field flex-column'}))
	review = forms.CharField(max_length=255, label='Review', widget=forms.TextInput(
		attrs={'class': 'form-field flex-column'}))

# update review juga sama