from time import gmtime, strftime
import datetime
import traceback
from django.core.paginator import Paginator
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.db import connection
from django.contrib import messages

# Create your views here.
from django.urls import reverse

from .forms import StatusFilter, Level, PemesananAnggota, PemesananAdmin
from michael.check_login import required_login
from michael.models import Pemesanan, LevelKeanggotaan, BarangPesanan


def make_order(request):
    if request.method == 'POST':
        no_ktp_anggota = request.session['no_ktp']
        if request.session['role'] == 'admin':
            form = PemesananAdmin(request.POST)
            if form.is_valid():
                no_ktp_anggota = form.cleaned_data.get('anggota')
        else:
            form = PemesananAnggota(request.POST)
        if form.is_valid():
            with connection.cursor() as cursor:
                try:
                    cursor.execute("SELECT COALESCE(max(id_pemesanan), '0') FROM PEMESANAN")
                    id_pemesanan = int(cursor.fetchone()[0]) + 1
                    cursor.execute("SELECT COALESCE(max(no_urut), '0') FROM BARANG_PESANAN")
                    no_urut = int(cursor.fetchone()[0]) + 1
                    datetime_pemesanan = strftime("%Y-%m-%d %H:%M:%S", gmtime())
                    kuantitas_barang = len(form.cleaned_data.get('barang'))
                    barang = form.cleaned_data.get('barang')
                    lama_sewa = form.cleaned_data.get('lama_sewa')
                    status = 'Dikonfirmasi'
                    tanggal_sewa = datetime.datetime.today()
                    data_pemesanan = [id_pemesanan, datetime_pemesanan, kuantitas_barang, no_ktp_anggota, status]
                    data_barang_pesanan = [id_pemesanan, no_urut, 0, tanggal_sewa, lama_sewa, status]
                    cursor.execute(
                        'INSERT INTO PEMESANAN (id_pemesanan, datetime_pemesanan, '
                        'kuantitas_barang, no_ktp_pemesan, status) VALUES (%s, %s, %s, %s, %s)',
                        data_pemesanan
                    )
                    for i in barang:
                        data_barang_pesanan[2] = i
                        data_barang_pesanan[1] = data_barang_pesanan[1] + 1
                        cursor.execute(
                            'INSERT INTO BARANG_PESANAN (id_pemesanan, no_urut, id_barang, '
                            'tanggal_sewa, lama_sewa, status) VALUES (%s, %s, %s, %s, %s, %s)',
                            data_barang_pesanan
                        )
                except Exception as ex:
                    messages.error(request, ex)
                    return HttpResponseRedirect(request.META['HTTP_REFERER'])
                return HttpResponseRedirect(reverse('order:list-order'))
    form = PemesananAnggota()
    if request.session['role'] == 'admin':
        form = PemesananAdmin()
    return render(request, 'jeremia/makeOrder.html', {'form': form})


def get_barang(id_pemesanan):
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT * FROM BARANG_PESANAN WHERE id_pemesanan=%s", [id_pemesanan]
        )
        row = cursor.fetchall()
        return row

@required_login
def edit_order(request, id_pemesanan):
    barang_pesanan = get_barang(id_pemesanan)
    pemesanan = Pemesanan.objects.raw("SELECT * FROM PEMESANAN "
                                      "WHERE id_pemesanan=%s", [id_pemesanan])
    initial_barang = [x[2] for x in barang_pesanan]
    if pemesanan:
        no_ktp_pemesan = pemesanan[0].no_ktp_pemesan
        initial_anggota = [no_ktp_pemesan.no_ktp.no_ktp]
    else:
        no_ktp_pemesan = None
        initial_anggota = None
    if barang_pesanan:
        initial_nama_sewa = barang_pesanan[0][4]
    else:
        initial_nama_sewa = None
    initial_pemesanan_anggota = {
        'barang': initial_barang,
        'lama_sewa': initial_nama_sewa
    }
    initial_pemesanan_admin = {
        'anggota': initial_anggota,
        'barang': initial_barang,
        'lama_sewa': initial_nama_sewa
    }
    with connection.cursor() as cursor:
        for i in initial_barang:
            cursor.execute("UPDATE BARANG SET kondisi='Baik' WHERE id_barang=%s", [i])
    if request.method == 'POST':
        no_ktp_anggota = no_ktp_pemesan
        if request.session['role'] == 'admin':
            form = PemesananAdmin(request.POST)
            if form.is_valid():
                no_ktp_anggota = form.cleaned_data.get('anggota')
        else:
            form = PemesananAnggota(request.POST)
        if form.is_valid():
            with connection.cursor() as cursor:
                try:
                    cursor.execute("SELECT COALESCE(max(no_urut), '0') FROM BARANG_PESANAN")
                    no_urut = int(cursor.fetchone()[0])
                    cursor.execute("DELETE FROM BARANG_PESANAN WHERE id_pemesanan=%s", [id_pemesanan])
                    kuantitas_barang = len(form.cleaned_data.get('barang'))
                    barang = form.cleaned_data.get('barang')
                    lama_sewa = form.cleaned_data.get('lama_sewa')
                    status = 'Dikonfirmasi'
                    tanggal_sewa = datetime.datetime.today()
                    data_pemesanan = [kuantitas_barang, no_ktp_anggota, id_pemesanan]
                    data_barang_pesanan = [id_pemesanan, no_urut, 0, tanggal_sewa, lama_sewa, status]
                    cursor.execute(
                        'UPDATE PEMESANAN SET kuantitas_barang=%s, no_ktp_pemesan=%s WHERE id_pemesanan=%s',
                        data_pemesanan
                    )
                    for i in barang:
                        data_barang_pesanan[2] = i
                        data_barang_pesanan[1] = data_barang_pesanan[1] + 1
                        cursor.execute(
                            'INSERT INTO BARANG_PESANAN (id_pemesanan, no_urut, id_barang, '
                            'tanggal_sewa, lama_sewa, status) VALUES (%s, %s, %s, %s, %s, %s)',
                            data_barang_pesanan
                        )
                except Exception as ex:
                    messages.error(request, ex)
                    return HttpResponseRedirect(request.META['HTTP_REFERER'])
                return HttpResponseRedirect(reverse('order:list-order'))
    if request.session['role'] == 'admin':
        return render(request, 'jeremia/makeOrder.html', {'form': PemesananAdmin(initial_pemesanan_admin)})
    elif request.session['role'] == 'anggota':
        return render(request, 'jeremia/makeOrder.html', {'form': PemesananAnggota(initial_pemesanan_anggota)})


@required_login
def list_order(request):
    form = StatusFilter(request.GET)
    if not form.is_valid():
        messages.error(request, "ERROR")
        return HttpResponseRedirect(request.META['HTTP_REFERER'])
    sort = form.cleaned_data.get('order')
    status = form.cleaned_data.get('filter_status')
    query = 'SELECT id_pemesanan, harga_sewa, status FROM PEMESANAN'
    if status:
        query += " WHERE status = '{}'".format(status)
        if request.session['role'] == 'anggota':
            query += " AND no_ktp_pemesan = '{}'".format(request.session['no_ktp'])
    else:
        if request.session['role'] == 'anggota':
            query += " WHERE no_ktp_pemesan = '{}'".format(request.session['no_ktp'])
    if sort:
        if sort == 'ascending':
            query += ' order by id_pemesanan ASC'
        elif sort == 'descending':
            query += ' order by id_pemesanan DESC'
    pemesanan = Pemesanan.objects.raw(query)
    p = Paginator(pemesanan, 10)
    page = request.GET.get("page")
    orders = p.get_page(page)
    with connection.cursor() as cursor:
        result = []
        for i in orders:
            cursor.execute('Select nama_item from BARANG B, BARANG_PESANAN BP where B.id_barang=BP.id_barang and '
                           'id_pemesanan = %s', [i.id_pemesanan])
            barang = cursor.fetchall()
            result.append((i.id_pemesanan, barang, i.harga_sewa, i.status.nama))
    return render(request, 'jeremia/listOrder.html', {'item': orders, 'result': result, 'form': form})


@required_login
def create_level(request):
    if request.session['role'] == 'anggota':
        return HttpResponseRedirect(reverse('order:list-order'))
    if request.method == 'POST':
        form = Level(request.POST)
        if form.is_valid():
            nama_level = form.cleaned_data.get('nama_level')
            minimum_poin = form.cleaned_data.get('minimum_poin')
            deskripsi = form.cleaned_data.get('deskripsi')
            data = [nama_level, minimum_poin, deskripsi]
            try:
                with connection.cursor() as cursor:
                    cursor.execute('INSERT INTO LEVEL_KEANGGOTAAN values (%s, %s, %s)', data)
            except Exception:
                messages.error(request, "Nama level tidak boleh duplikat")
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            return HttpResponseRedirect(reverse('level:list-level'))
    form = Level()
    return render(request, 'jeremia/createLevel.html', {'form': form})


@required_login
def update_level(request, nama_level):
    if request.session['role'] == 'anggota':
        return HttpResponseRedirect(reverse('order:list-order'))
    level = LevelKeanggotaan.objects.raw("SELECT * FROM LEVEL_KEANGGOTAAN "
                                         "WHERE nama_level=%s", [nama_level])
    initial_level = {
        'nama_level': level[0].nama_level,
        'minimum_poin': level[0].minimum_poin,
        'deskripsi': level[0].deskripsi
    }
    if request.method == 'POST':
        form = Level(request.POST)
        if form.is_valid():
            nama_level = form.cleaned_data.get('nama_level')
            minimum_poin = form.cleaned_data.get('minimum_poin')
            deskripsi = form.cleaned_data.get('deskripsi')
            data = [nama_level, minimum_poin, deskripsi, initial_level['nama_level']]
            try:
                with connection.cursor() as cursor:
                    cursor.execute('UPDATE LEVEL_KEANGGOTAAN SET '
                                   'nama_level = %s, '
                                   'minimum_poin = %s, '
                                   'deskripsi = %s '
                                   'WHERE nama_level = %s', data)
            except Exception:
                messages.error(request, "Nama level tidak boleh duplikat")
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            return HttpResponseRedirect(reverse('level:list-level'))
    form = Level(initial=initial_level)
    return render(request, 'jeremia/createLevel.html', {'form': form})


@required_login
def list_level(request):
    query = 'SELECT * FROM LEVEL_KEANGGOTAAN ORDER BY minimum_poin DESC'
    level = LevelKeanggotaan.objects.raw(query)
    result = []
    for i in level:
        result.append((i.nama_level, i.deskripsi, i.minimum_poin))
    return render(request, 'jeremia/listLevel.html', {'result': result})


@required_login
def remove_order(request, id_pemesanan):
    if request.session['role'] == 'admin':
        return HttpResponseRedirect(reverse('order:list-order'))
    with connection.cursor() as cursor:
        cursor.execute(
            'DELETE FROM PEMESANAN where id_pemesanan = %s', [id_pemesanan]
        )
    return HttpResponseRedirect(reverse('order:list-order'))


@required_login
def remove_level(request, nama_level):
    if request.session['role'] == 'anggota':
        return HttpResponseRedirect(reverse('level:list-level'))
    with connection.cursor() as cursor:
        cursor.execute(
            'DELETE FROM LEVEL_KEANGGOTAAN where nama_level = %s', [nama_level]
        )
    return HttpResponseRedirect(reverse('level:list-level'))
