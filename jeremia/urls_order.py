from django.urls import path
app_name = 'order'
from .views import make_order, list_order, remove_order, edit_order

urlpatterns = [
    path('make', make_order, name='make-order'),
    path('list', list_order, name='list-order'),
    path('edit/<str:id_pemesanan>', edit_order, name='edit-order'),
    path('delete/<str:id_pemesanan>', remove_order, name='delete-order'),
]