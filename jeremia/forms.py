from django import forms
from django.db import connection
from django_select2.forms import Select2MultipleWidget

from michael.models import Barang


def list_status():
    with connection.cursor() as cursor:
        cursor.execute(
            'SELECT nama, nama from status'
        )
        row = cursor.fetchall()
        row.append(("", ""))
    return row


class StatusFilter(forms.Form):
    filter_status = forms.ChoiceField(required=False, label="Filter Status",
                                      widget=forms.Select(attrs={'class': 'form-control', 'id': 'sel1'}),
                                      choices=list_status())
    order = forms.ChoiceField(required=False, label="Order by ID",
                              widget=forms.Select(attrs={'class': 'form-control', 'id': 'sel1'}),
                              choices=[('', ''), ('ascending', 'ascending'), ('descending', 'descending')])


def list_choices_barang():
    result = Barang.objects.raw("SELECT id_barang, nama_item, warna FROM BARANG WHERE kondisi='Baik'")
    cleaned = [[i.id_barang, i.nama_item.nama + ", color: " + i.warna] for i in result]
    return cleaned


def list_anggota():
    with connection.cursor() as cursor:
        cursor.execute(
            'SELECT P.no_ktp, nama_lengkap FROM PENGGUNA P, ANGGOTA A '
            'WHERE P.no_ktp=A.no_ktp'
        )
        row = cursor.fetchall()
    return row


class PemesananAdmin(forms.Form):
    anggota = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}),
                                choices=list_anggota())
    barang = forms.MultipleChoiceField(required=True, choices=list_choices_barang,
                                       widget=Select2MultipleWidget(attrs={'class': 'form-field flex-column'}))
    lama_sewa = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-field flex-column'}),
                                   required=True)


class PemesananAnggota(forms.Form):
    barang = forms.MultipleChoiceField(required=True, choices=list_choices_barang,
                                       widget=Select2MultipleWidget(attrs={'class': 'form-field flex-column'}))
    lama_sewa = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-field flex-column'}),
                                   required=True)


class Level(forms.Form):
    nama_level = forms.CharField(max_length=20, widget=forms.TextInput(attrs={'class': 'form-field flex-column'}),
                                 required=True)
    minimum_poin = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-field flex-column'}),
                                      required=True)
    deskripsi = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'class': 'form-field flex-column'}),
                                required=False)
