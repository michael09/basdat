from django.urls import path
app_name = 'level'
from .views import create_level, list_level, remove_level, update_level

urlpatterns = [
    path('create', create_level, name='create-level'),
    path('list', list_level, name='list-level'),
    path('edit/<str:nama_level>', update_level, name='edit-level'),
    path('delete/<str:nama_level>', remove_level, name='delete-level'),
]