from django import forms
from django_select2.forms import Select2MultipleWidget
from .models import Kategori
from django.core.validators import RegexValidator
validate_regex = RegexValidator(r'[0-9]-[0-9]')


class LoginForm(forms.Form):
    nomor_ktp = forms.CharField(max_length=200, widget=forms.NumberInput(attrs={'class': 'form-field flex-column'}))
    email = forms.EmailField()


class RegistrationForm(forms.Form):
    nomor_ktp = forms.CharField(max_length=200, widget=forms.NumberInput(attrs={'class': 'form-field flex-column'}))
    nama_lengkap = forms.CharField(max_length=300, widget=forms.TextInput(attrs={'class': 'form-field flex-column'}))
    email = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'form-field flex-column'}))
    tanggal_lahir = forms.DateField(widget=forms.DateInput(attrs={'type': 'date'}), required=False)
    nomor_telepon = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-field flex-column'}), required=False)


class FormAddress(forms.Form):
    nama = forms.CharField(max_length=20, widget=forms.TextInput(attrs={'class': 'form-field flex-column'}))
    jalan = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'class': 'form-field flex-column'}))
    nomor = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-field flex-column'}))
    kota = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'class': 'form-field flex-column'}))
    kodepos = forms.CharField(max_length=10, widget=forms.TextInput(attrs={'class': 'form-field flex-column'}))


def list_choices_kategori():
    result = Kategori.objects.raw("SELECT nama from KATEGORI")
    cleaned = [[i.nama, i.nama] for i in result]
    return cleaned


class Items(forms.Form):
    Nama = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'class': 'form-field flex-column'}))
    Deskripsi = forms.CharField(required=False, max_length=10000, widget=forms.Textarea(attrs={'class': 'form-field flex-column',
                                                                               'rows': "3",
                                                                               'colums': "9"}))
    Rentang_usia = forms.CharField(max_length=200, label="Rentang usia", widget=forms.TextInput(attrs={'class': 'form-field flex-column'})
                                   , validators=[validate_regex])
    Bahan = forms.CharField(required=False, max_length=255, widget=forms.TextInput(attrs={'class': 'form-field flex-column'}))
    Kategori = forms.MultipleChoiceField(required=False, choices=list_choices_kategori,
                                         widget=Select2MultipleWidget(attrs={'class': 'form-field flex-column'}))
