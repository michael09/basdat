from django.urls import path
from .views import list_items, create_items, update_items, remove_item
app_name = 'items'

urlpatterns = [
    path('list-items', list_items, name='list-item'),
    path('add-item', create_items, name='add-item'),
    path('edit-item/<str:nama>', update_items, name='edit-item'),
    path('delete/<str:nama>', remove_item, name='delete-item')
]
