from django.forms import formset_factory
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from .forms import LoginForm, RegistrationForm, Items, FormAddress
from django.db import connection
from michael.check_login import required_login
from .models import Pengguna, Anggota, Item, KategoriItem
from django.contrib import messages
from django.core.paginator import Paginator
from stevany.forms import order_and_filter
# Create your views here.


def registration_admin(form):
    result = [form.cleaned_data.get('nomor_ktp'), form.cleaned_data.get('nama_lengkap'),
              form.cleaned_data.get('email'), form.cleaned_data.get('tanggal_lahir'),
              form.cleaned_data.get('nomor_telepon')]
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO PENGGUNA values(%s, %s, %s, %s, %s)", result)
        cursor.execute("INSERT INTO ADMIN values(%s)", [form.cleaned_data.get('nomor_ktp'), ])


def registration_anggota(form, address):
    result = [form.cleaned_data.get('nomor_ktp'), form.cleaned_data.get('nama_lengkap'),
              form.cleaned_data.get('email'), form.cleaned_data.get('tanggal_lahir'),
              form.cleaned_data.get('nomor_telepon')]
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO PENGGUNA values(%s, %s, %s, %s, %s)", result)
        cursor.execute("INSERT INTO ANGGOTA values(%s, 0,'Bronze')", [form.cleaned_data.get('nomor_ktp'), ])
        for address_form in address.forms:
            result_address = [
                form.cleaned_data.get('nomor_ktp'),
                address_form.cleaned_data.get('nama'),
                address_form.cleaned_data.get('jalan'),
                address_form.cleaned_data.get('nomor'),
                address_form.cleaned_data.get('kota'),
                address_form.cleaned_data.get('kodepos')
            ]
            cursor.execute("INSERT INTO ALAMAT values(%s, %s, %s, %s, %s, %s)", result_address)


def registration(request):
    if 'role' in request.session:
        if request.session['role'] == 'admin':
            return HttpResponseRedirect(reverse('order:list-order'))
        else:
            return HttpResponseRedirect(reverse('toys:toys_list'))
    role = request.GET.get('role')
    form_address = formset_factory(FormAddress, extra=1)
    if role == 'admin' or role == 'anggota':
        if request.method == 'POST':
            form = RegistrationForm(request.POST, prefix="form")
            if form.is_valid():
                no_ktp_exist = list(Pengguna.objects.raw('SELECT * FROM PENGGUNA WHERE no_ktp = %s',
                                                         [form.cleaned_data.get('nomor_ktp'), ]))
                if no_ktp_exist:
                    messages.error(request, 'No KTP sudah terdaftar')
                email_exist = list(
                    Pengguna.objects.raw('SELECT * FROM PENGGUNA WHERE email = %s', [form.cleaned_data.get('email'), ]))
                if email_exist:
                    messages.error(request, 'Email sudah terdaftar')
                if email_exist or no_ktp_exist:
                    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
                if role == 'anggota':
                    address = form_address(request.POST, prefix="address")
                    if address.is_valid():
                        registration_anggota(form, address)
                        return HttpResponseRedirect(reverse('auth:login'))
                elif role == 'admin':
                    registration_admin(form)
                    return HttpResponseRedirect(reverse('auth:login'))
            messages.error(request, 'Form tidak valid')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        form = RegistrationForm(prefix="form")
        if role == 'anggota':
            form_address = form_address(prefix="address")
        else:
            form_address = None
    else:
        return render(request, 'michael/choose.html')
    return render(request, 'michael/registration.html', {'form': form, 'adress': form_address})


def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            no_ktp = form.cleaned_data.get('nomor_ktp')
            email = form.cleaned_data.get('email')
            data = [no_ktp, email]
            user = Pengguna.objects.raw('SELECT * FROM PENGGUNA WHERE no_ktp = %s and email = %s', data)
            if list(user):
                request.session['no_ktp'] = no_ktp
                if list(Anggota.objects.raw('SELECT * FROM ANGGOTA WHERE no_ktp = %s', [no_ktp, ])):
                    request.session['role'] = 'anggota'
                    return HttpResponseRedirect(reverse('toys:toys_list'))
                else:
                    request.session['role'] = 'admin'
                    return HttpResponseRedirect(reverse('order:list-order'))
            else:
                messages.error(request, 'Email atau Nomor KTP salah')
        else:
            messages.error(request, 'Form tidak valid!')
    if 'role' in request.session:
        if request.session['role'] == 'admin':
            return HttpResponseRedirect(reverse('order:list-order'))
        else:
            return HttpResponseRedirect(reverse('toys:toys_list'))
    return render(request, 'michael/login.html')


@required_login
def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('registration'))


@required_login
def list_items(request):
    form = order_and_filter(request.GET)
    if not form.is_valid():
        messages.error(request, "ERROR")
        return HttpResponseRedirect(request.META['HTTP_REFERER'])
    query = 'SELECT nama FROM ITEM'
    order = form.cleaned_data.get('order')
    category = form.cleaned_data.get('filter_category')
    if category:
        query = 'SELECT nama from ITEM, Kategori_item WHERE nama_item = nama and nama_kategori = %s'
    if order == 'ascending':
        query += ' order by nama ASC'
    elif order == 'descending':
        query += ' order by nama DESC'
    if category:
        item = Item.objects.raw(query, [category])
    else:
        item = Item.objects.raw(query)
    p = Paginator(item, 10)
    page = request.GET.get("page")
    items = p.get_page(page)
    with connection.cursor() as cursor:
        result = []
        for i in items:
            cursor.execute('Select nama_kategori from kategori_item where nama_item = %s', [i.nama])
            kategori = cursor.fetchall()
            result.append((i.nama, kategori))
    return render(request, 'michael/list-item.html', {'item': items, 'result': result, 'form': form})


def save_item(form):
    result = []
    nama = form.cleaned_data.get('Nama')
    deskripsi = form.cleaned_data.get('Deskripsi')
    rentang = form.cleaned_data.get('Rentang_usia').split("-")
    usia_dari = rentang[0]
    usia_sampai = rentang[1]
    bahan = form.cleaned_data.get('Bahan')
    result.append(nama)
    result.append(deskripsi)
    result.append(usia_dari)
    result.append(usia_sampai)
    result.append(bahan)
    kategori = form.cleaned_data.get('Kategori')
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO ITEM values(%s, %s, %s, %s, %s)", result)
        for k in kategori:
            cursor.execute("INSERT INTO kategori_item values(%s, %s)", [nama, k])


@required_login
def create_items(request):
    if request.session['role'] == 'user':
        return HttpResponseRedirect(reverse('items:list-item'))
    if request.method == 'POST':
        form = Items(request.POST)
        if form.is_valid():
            save_item(form)
            return HttpResponseRedirect(reverse('items:list-item'))
        else:
            messages.error(request, "Form salah!")
            return HttpResponseRedirect(reverse('items:add-item'))
    form = Items()
    return render(request, 'michael/create-update-item.html', {'form': form})


def get_kategori(nama):
    with connection.cursor() as cursor:
        cursor.execute(
            'SELECT nama_kategori from kategori_item where nama_item = %s', [nama, ])
        row = cursor.fetchall()
        return row


def delete_item(nama):
    with connection.cursor() as cursor:
        cursor.execute(
            'DELETE FROM item where nama = %s', [nama]
        )

@required_login
def remove_item(request, nama):
    if request.session['role'] == 'user':
        return HttpResponseRedirect(reverse('items:list-item'))
    delete_item(nama)
    return HttpResponseRedirect(reverse('items:list-item'))


def intersection(lst1, lst2):
    # Use of hybrid method
    temp = set(lst2)
    lst3 = [value for value in lst1 if value in temp]
    return lst3


def Diff(li1, li2):
    return list(set(li1) - set(li2))


@required_login
def update_items(request, nama):
    if request.session['role'] == 'user':
        return HttpResponseRedirect(reverse('items:list-item'))
    item = Item.objects.raw('SELECT * FROM ITEM WHERE nama = %s', [nama, ])
    kategori = get_kategori(nama)
    kategori = [x[0] for x in kategori]
    data = {
        'Nama': item[0].nama,
        'Deskripsi': item[0].deskripsi,
        'Rentang_usia': str(item[0].usia_dari) + "-" + str(item[0].usia_sampai),
        'Bahan': item[0].bahan,
        'Kategori': kategori
    }
    if request.method == 'POST':
        form = Items(request.POST, initial=data)
        if form.is_valid() and form.has_changed():
            with connection.cursor() as cursor:
                for c in form.changed_data:
                    nama = form.cleaned_data.get('Nama')
                    if c == 'Rentang_usia':
                        rentang = form.cleaned_data.get('Rentang_usia').split("-")
                        cursor.execute('UPDATE item set usia_dari = %s WHERE nama = %s', [rentang[0], nama])
                        cursor.execute('UPDATE item set usia_sampai = %s WHERE nama = %s', [rentang[1], nama])
                    elif c == 'Kategori':
                        intersect = intersection(data['Kategori'], form.cleaned_data.get("Kategori"))
                        deleted = Diff(data['Kategori'], intersect)
                        added = Diff(form.cleaned_data.get("Kategori"), intersect)
                        for d in deleted:
                            cursor.execute('DELETE FROM kategori_item where nama_item = %s and nama_kategori = %s', [nama, d])
                        for a in added:
                            cursor.execute('INSERT INTO kategori_item values (%s, %s)', [nama, a])
                    else:
                        if c == 'Nama':
                            nama = data["Nama"]
                        data_ganti = [form.cleaned_data.get(c), nama]
                        cursor.execute(
                            'UPDATE item set %s = %s WHERE nama = %s' % (c, '%s', '%s'), data_ganti
                        )
        return HttpResponseRedirect(reverse('items:list-item'))
    form = Items(data)
    return render(request, 'michael/create-update-item.html', {'form': form})


@required_login
def profile(request):
    data = [request.session['no_ktp']]
    user = Pengguna.objects.raw('SELECT * FROM PENGGUNA WHERE no_ktp = %s', data)
    data_level = None
    if list(user) and len(user) < 2:
        anggota = Anggota.objects.raw('SELECT * FROM ANGGOTA WHERE no_ktp = %s', data)
        if list(anggota):
            data_level = {'poin': anggota[0].poin, 'level': anggota[0].level.nama_level}
    else:
        messages.error(request, 'Profile error please log in again')
        return HttpResponseRedirect(reverse('auth:login'))
    form = RegistrationForm(initial={
        'nomor_ktp': user[0].no_ktp,
        'nama_lengkap': user[0].nama_lengkap,
        'email': user[0].email,
        'tanggal_lahir': user[0].tanggal_lahir,
        'nomor_telepon': user[0].no_telp,
    })
    for f in form:
        f.field.disabled = True
    return render(request, 'michael/profile.html', {'form': form, 'data_level': data_level})