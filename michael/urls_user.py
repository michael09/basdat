from django.urls import path
app_name = 'auth'
from .views import login, logout, profile

urlpatterns = [
    path('login', login, name='login'),
    path('logout', logout, name='logout'),
    path('profile', profile, name='profile')
]