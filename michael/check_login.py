from django.http import HttpResponseRedirect
from django.urls import reverse


def required_login(function):
    def _function(request, *args, **kwargs):
        if 'role' not in request.session:
            return HttpResponseRedirect(reverse('auth:login'))
        return function(request, *args, **kwargs)
    return _function
