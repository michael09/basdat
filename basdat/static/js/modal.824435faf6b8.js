function displayModal(element) {
    let id = element.id;
    let selector = `#modal-${id}`;
    $(selector).modal();
}