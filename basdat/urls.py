"""basdat URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# from django.contrib import admin
from django.conf.urls import url
from django.urls import path, include
from michael.views import registration
urlpatterns = [
    url(r'^select2/', include('django_select2.urls')),
    path('', registration, name="registration"),
    path('user/', include('michael.urls_user', namespace='auth')),
    path('item/', include('michael.urls_item', namespace="items")),
    path('chat/', include('stevany.urls_chat', namespace='chat')),
    path('toys/', include('stevany.urls_toys', namespace='toys')),
    path('order/', include('jeremia.urls_order', namespace='order')),
    path('level/', include('jeremia.urls_level', namespace='level')),
    path('shipping/', include('Noor.urls_shipping', namespace='shipping')),
    path('review/', include('Noor.urls_review', namespace='review'))
]
